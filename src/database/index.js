import mongoose from "mongoose";
import { mongoUri } from "../env.js";

export const connect = (dbName) => {
  return mongoose.connect(mongoUri, {
    dbName: dbName ? dbName : "prod_database",
  });
};

export const disconnect = () => {
  return mongoose.disconnect();
};
